import 'package:first_project_flutter/screens/Questions/one.dart';
import 'package:flutter/material.dart';

//widget
class NameScreen extends StatefulWidget {
  @override
  _NameScreenState createState() => _NameScreenState();
}

//state of the widget
class _NameScreenState extends State<NameScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey();

  void _Submit() {}
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        body: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [Colors.amberAccent, Colors.redAccent])),
            child: Stack(children: <Widget>[
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  //crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Positioned(
                        child: Image.asset(
                      "assets/images/Name.png",
                      alignment: Alignment.centerLeft,
                      height: size.height * 0.3,
                      width: size.width * 0.5,
                    )),
                    //SizedBox(height: 15),
                    Center(
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Container(
                          height: 220,
                          width: 300,
                          padding: EdgeInsets.all(15),
                          child: Form(
                            key: _formKey,
                            child: SingleChildScrollView(
                              child: Column(
                                children: <Widget>[
                                  TextFormField(
                                    decoration: InputDecoration(
                                        labelText: 'First Name'),
                                    keyboardType: TextInputType.name,
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return 'Enter Your First Name';
                                      }
                                      return null;
                                    },
                                    onSaved: (value) {},
                                  ),
                                  TextFormField(
                                    decoration:
                                        InputDecoration(labelText: 'Last Name'),
                                    keyboardType: TextInputType.name,
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return 'Enter Your Last Name';
                                      }
                                      return null;
                                    },
                                    onSaved: (value) {},
                                  ),
                                  SizedBox(
                                    height: 30,
                                  ),
                                  RaisedButton(
                                    child: Text('Submit'),
                                    onPressed: () {
                                      _Submit();
                                    },
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(30),
                                    ),
                                    color: Colors.orangeAccent,
                                    textColor: Colors.black,
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    //SizedBox(height: 15),
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: MaterialButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => OneScreen()));
                          //Navigator.push(context, MaterialPageRoute(builder: (context) => OneScreen()),
                        },
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30)),
                        color: Colors.orangeAccent,
                        padding: EdgeInsets.all(15),
                        child: Text(" NEXT ",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                                fontWeight: FontWeight.bold)),
                      ),
                    )
                  ],
                ),
              )
            ])));
  }
}
